import React, { Component } from 'react';
import { View, Text,Image, Linking } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid'
import { Content, Spinner, Card, CardItem, Header, Tabs, ScrollableTab, Tab, Container, ListItem, List, Button,  } from 'native-base';
import TitleHeader from '../../layouts/TitleHeader';
import { AirbnbRating } from 'react-native-ratings';


export default class ViewPhone extends Component {
  constructor(props) {
    
    super(props)
    this.state = {
      phone: {make:'',link:'',profile_pic:'http://gbaproducts.com/wp-content/uploads/2017/11/img-placeholder-dark-vertical.jpg',model:'',price:'',title:''}
    }
  }
  _getRandomInt = (max)=> {
    return Math.floor(Math.random() * Math.floor(max))+1;
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  componentDidMount = () => {
    this.subs = [
      this.props.navigation.addListener("didFocus", () => this.setState({ isFocused: true },()=>{
        let phone = this.props.navigation.getParam('phone',{make:'',link:'',profile_pic:'http://gbaproducts.com/wp-content/uploads/2017/11/img-placeholder-dark-vertical.jpg',model:'',price:'',title:''})
        console.log(phone)
        this.setState({phone:{make:'',link:'',profile_pic:'http://gbaproducts.com/wp-content/uploads/2017/11/img-placeholder-dark-vertical.jpg',model:'',price:'',title:''}},()=>{
          this.setState({phone:phone})
        })
      })),
      this.props.navigation.addListener("willBlur", () => this.setState({ isFocused: false },()=>{
        this.props.navigation.setParams({phone:{make:'',link:'',profile_pic:'http://gbaproducts.com/wp-content/uploads/2017/11/img-placeholder-dark-vertical.jpg',model:'',price:'',title:''}})
      }))
    ]
    
  };
  

  render() {
    const {width,height} = this.props.screenProps
    const {assets} = this.props.screenProps.stores
    return (        
            <TitleHeader showMenu={false} title={`Phone Details`} {...this.props}>
              <Content style={{minWidth:width,backgroundColor:'rgba(0,0,0,.9)'}}>
                  <Image source={{uri: this.state.phone.profile_pic}} resizeMode={'stretch'} style={{alignSelf:'center',width:width*.96,height:height*.4,maxHeight:height*.4,}} />
                  <Image source={{uri: this.state.phone.profile_pic}} resizeMode={'stretch'} style={{zIndex:100,alignSelf:'center',width:width*.3,height:width*35,maxHeight:width*.35,position:'absolute',top:height*.25,right:20,}} />
                <List>
                  <ListItem style={{backgroundColor:'wheat',height:height*.06,margin:10}} itemDivider><Text style={{color:'#000',fontWeight:'bold',fontSize:20,fontStyle:'italic'}} >Title</Text></ListItem>
                  <Text style={{color:'#fff',fontWeight:'bold',fontSize:17,fontFamily:'monospace'}} >{this.state.phone.model}</Text>
                  <ListItem style={{backgroundColor:'wheat',height:height*.06,margin:10}} itemDivider><Text style={{color:'#000',fontWeight:'bold',fontSize:20,fontStyle:'italic'}} >Price</Text></ListItem>
                  <Text style={{color:'#fff',fontWeight:'bold',fontSize:28,alignSelf:'center'}} >{this.state.phone.price}</Text>
                  <ListItem style={{backgroundColor:'wheat',height:height*.06,margin:10}} itemDivider><Text style={{color:'#000',fontWeight:'bold',fontSize:20,fontStyle:'italic'}} >Rating</Text></ListItem>
                  <AirbnbRating
                    count={5}
                    reviews={["Terrible", "Meh", "OK", "Good", "Cheesus!"]}
                    defaultRating={this._getRandomInt(4)}
                    size={15}
                  />
                  <ListItem style={{backgroundColor:'wheat',height:height*.06,margin:10}} itemDivider><Text style={{color:'#000',fontWeight:'bold',fontSize:20,fontStyle:'italic'}} >Description</Text></ListItem>
                  <Text style={{color:'#fff',fontWeight:'bold',fontSize:19,fontFamily:'serif'}} >SPECS:   
                    <Text style={{color:'#f5f5f5',fontStyle:'italic', fontSize:15}} >{this.state.phone.title}</Text>
                  </Text>
                  <ListItem style={{backgroundColor:'wheat',height:height*.06,margin:10}} itemDivider><Text style={{color:'#000',fontWeight:'bold',fontSize:20,fontStyle:'italic'}} >Purchase</Text></ListItem>
                  <Button rounded bordered block warning onPress={()=>{Linking.openURL(this.state.phone.link)}} >
                    <Text style={{color:'#fff',fontWeight:'bold',fontSize:20,fontStyle:'italic'}} >Add to Cart</Text>
                  </Button>
                </List>

              </Content>
            </TitleHeader>

    );
  }
}
