import React, { Component } from 'react'
import { View, Text, Image, AsyncStorage, TouchableOpacity, FlatList } from 'react-native'
import TitleHeader from '../../layouts/TitleHeader'
import { Content, Spinner, Card, CardItem, Header, Tabs, ScrollableTab, Tab} from 'native-base'
import firebase from 'react-native-firebase'
import {NavigationActions, StackActions} from 'react-navigation'
import StarRating from 'react-native-star-rating'
import {  LazyloadListView,LazyloadScrollView,  LazyloadView} from 'react-native-lazyload'
import { List, ListItem } from 'react-native-elements';

export default class StoreScene extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tabs_elements:[],
      fetching_complete:false,
      error:'',
      fetched_data:'',
      cached_data:'',
      isFocused:false,
    }
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

  componentDidMount = () => {
    // this.subs = [
    //   this.props.navigation.addListener("didFocus", () => this.setState({ isFocused: true },()=>{
        this._initializeFetch()
        .then((response)=>{
          console.log('First five tabs arrived ',response.initial_tabs)
          this.setState({tabs_elements:response.initial_tabs,fetching_complete:true},()=>{
            this.dataRefresher = setInterval(()=>{
              if(this.state.cached_data == this.state.fetched_data){
                console.log('cached data is the same as fetched data')
                clearInterval(this.dataRefresher)
              }else{
                console.log('cached data is not the same as fetched data')
                this._fetchData()
              }
              
            },10000)
            this._completeFetching(response.all_brands)
          })
        })
        .catch((error)=>{
          console.log('Error occured initializing : ',error)
          // this.setState({error:error})
          this._fetchData()
        }) 
    //   })),
    //   this.props.navigation.addListener("willBlur", () => this.setState({ isFocused: false },()=>{
    //       this.setState({fetching_complete:false})
    //   }))
    // ]
   
  }

  _initializeFetch(){
    console.log("Initializing loadout")
    return new Promise(async (resolve,reject)=>{
      await AsyncStorage.getItem('phones_data',(err,res)=>{
        if(!err && res){
          console.log("Found Cached data")
          this.setState({cached_data:res})          
          let phone_brands = JSON.parse(res)
          console.log(phone_brands)
          let count = 0
          let tabs = []
          for (let brand in phone_brands){
            count++
            let tab_elem = this._buldTabs({brand_name:brand,brand_phones:phone_brands[brand]})
            // console.log(brand)
            tabs.push(tab_elem)
            if(count >= 5){
              console.log('Completed with the first 5 tabs')
              resolve({initial_tabs:tabs,all_brands:phone_brands})
              break
            }
          }
        }else{
          console.log("No Cached data")
          reject(err)
        }
      })
    })
  }

  async _completeFetching(all_brands){
    console.log("Completing Fetch for all "+Object.keys(all_brands).length+" brands")    
    let tabs = []
    let count = 0
    for (let brand in all_brands){
      count++
      let tab_elem = this._buldTabs({brand_name:brand,brand_phones:all_brands[brand]})
      console.log('current brand in competion '+count+' ',brand)
      tabs.push(tab_elem)
      if(count == Object.keys(all_brands).length){
        this.setState({tabs_elements:[]},()=>{
          console.log('Completed the full fetch')
          this.setState({tabs_elements:tabs})
          this.forceUpdate()
        })
      }
    }
  }

  _fetchData(){
    console.log("Beginning Firebase Fetch")
    firebase.database().ref('smartphones/brands/').on('value',(brands_snap)=>{
      if(brands_snap.val()){
        console.log("Done Firebase Fetch")
        let phone_brands = brands_snap.val()
        let phone_brands_str = JSON.stringify(phone_brands)
        AsyncStorage.setItem('phones_data',phone_brands_str)
        this.setState({fetched_data:phone_brands_str,cached_data:phone_brands_str})
        this._initializeFetch()
        .then((response)=>{
          console.log('First five tabs arrived ',response.initial_tabs)
          this.setState({tabs_elements:response.initial_tabs,fetching_complete:true},()=>{
            this._completeFetching(response.all_brands)
          })
        })
        .catch((error)=>{
          console.log('Error occured initializing : ',error)
          this.setState({error:error})
        })
      }else{
        console.log("Failed Firebase Fetch")
      }
    })
  }

  _buldTabs({brand_name,brand_phones}){
    const {width,height} = this.props.screenProps 
    let left_phones = []
    let right_phones = []
    let side = 'left'
    for(let key in brand_phones){
      if(side == 'left'){
        // let phone_component = this._buldComponent(brand_phones[key])
        left_phones.push(brand_phones[key])
        side = 'right'
      }else{
        // let phone_component = this._buldComponent(brand_phones[key])
        right_phones.push(brand_phones[key])
        side = 'left'
      }
    }
    left_phones.length = right_phones.length
    // console.log(left_phones)
    return <Tab heading={brand_name} key={brand_name}
                tabStyle={{backgroundColor: 'rgba(0,0,0,.9)',borderRightColor:'gray',borderRightWidth:1,}} 
                activeTabStyle={{backgroundColor: 'black',borderRightColor:'gray',borderRightWidth:1,}}
                activeTextStyle={{color:'#daa521',fontSize:18,fontWeight:'bold'}}
                textStyle={{color:'white',fontSize:15}}
              >              
              <Content
                contentContainerStyle={{flexDirection:'row',maxWidth:width,minWidth:width,width:width,backgroundColor:'rgba(0,0,0,.8)',}}
              >
                <FlatList
                  name={'leftView'} 
                  style={{flex:1, maxWidth:width*.5,minWidth:width*.5}}
                  data={left_phones}
                  renderItem={this._buldComponent.bind(this)}
                  keyExtractor={(item, index) => index.toString()}
                />                   
                
                <FlatList
                  name={'rightView'} 
                  style={{flex:1, maxWidth:width*.5,minWidth:width*.5}}
                  data={right_phones}
                  renderItem={this._buldComponent.bind(this)}
                  keyExtractor={(item, index) => index.toString()}
                />
                    
              </Content>
            </Tab>
  }

  _buldComponent(phone_obj){
    // let phone_obj = {link,make,model,price,profile_pic,title}
    // console.log('logging the current phone: ',phone_obj.item)
    let that = this
    const {width,height} = this.props.screenProps 
    const {assets} = this.props.screenProps.stores

    let phone_title = phone_obj.item.model.split(' ')
    phone_title.length = 4
    phone_title = phone_title.join(' ')
    let   _getRandomInt = (max)=> {
      return Math.floor(Math.random() * Math.floor(max)) + 1
    }
    return <TouchableOpacity style={{margin:5,minWidth:width*.45,}} onPress={()=>{that._viewPhone(phone_obj.item)}} >
              <Image 
                source={{uri: phone_obj.item.profile_pic}}
                resizeMode={'stretch'}
                style={{width:width*.45,height:width*.55}}
              />
              <View>
                <Text style={{color:'#f5f5f5', fontWeight:'bold'}} numberOfLines={3} >{phone_title}</Text>
              </View>
              <View>
                <StarRating
                  starSize={20}
                  containerStyle={{backgroundColor:'transparent'}}
                  fullStarColor={'#daa521'}
                  emptyStarColor={'transparent'}
                  disabled={true}
                  maxStars={5}
                  rating={_getRandomInt(4)}
                />
              </View>
            </TouchableOpacity>
  }

  _viewPhone(phone_data){
    this.props.navigation.dispatch(
      NavigationActions.navigate({
        routeName: 'View',
        params: {phone:phone_data}
      })
    )
  }
  

  render() {
    const {width,height} = this.props.screenProps
    const {assets} = this.props.screenProps.stores
    return (
      <TitleHeader showMenu={true} title={'Store'} {...this.props}  >
        {/* <Header hasTabs /> */}
        {this.state.fetching_complete?          
            <Tabs prerenderingSiblingsNumber={2} renderTabBar={()=> <ScrollableTab />} style={{minWidth:width,backgroundColor:'#000'}} tabsContainerStyle={{backgroundColor:'#000'}} >
              {this.state.tabs_elements.map((tab,index,arr)=>tab)}
            </Tabs>
          :
          <Content contentContainerStyle={{backgroundColor:'rgba(0,0,0,.7)',minHeight:height,minWidth:width}} >
            {(this.state.error.length == 0)?
              <Spinner color={'orange'} style={{position:'absolute',top:height*.3,right:width*.45}} color={'orange'}  />
              :
              <Text style={{color:'#f5f5f5',alignSelf:'center',marginTop:height*.4}} >The following Error occured while fetching the data. {this.state.error.toString()}</Text>
            }
          </Content>
        }
      </TitleHeader>
    )
  }
}
