import React, { Component } from 'react';
import { ImageBackground, Image, AsyncStorage } from 'react-native';
import {Container, Spinner, StyleProvider} from 'native-base'
import {NavigationActions,StackActions} from 'react-navigation'
import firebase from 'react-native-firebase';

export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount = () => {
    const{settings,assets} = this.props.screenProps.stores
    setTimeout(()=>{
      this.props.navigation.navigate('Home')
    },settings.SplashTime)
    
  };
  
 
  
  render() {
    const {width,height,theme} = this.props.screenProps
    const{settings,assets} = this.props.screenProps.stores
    return (
      <StyleProvider style={theme}>
      <Container style={{backgroundColor:'#111111'}} >
        
        <ImageBackground source={assets.SplashBg} resizeMode={'cover'} style={{width:width,height:height,position:'absolute'}} >
          <Image source={assets.AppLogo} resizeMode={'contain'} style={{alignSelf:'center',width:100,height:100,marginTop:height*.2}} />
        </ImageBackground>
      </Container>
      </StyleProvider>
    );
  }
}
