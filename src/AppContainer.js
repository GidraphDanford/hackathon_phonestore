import React, { Component } from 'react'
import { Dimensions, View, Text, AsyncStorage, YellowBox,Platform } from 'react-native'
import getTheme from './native-base-theme/components'
import platform from './native-base-theme/variables/platform'
import firebase from 'react-native-firebase'
import {Root} from 'native-base'
import NavigationContainer from './NavigationContainer'
import NavigationService from './NavigationService'
import SetStore from './stores/SetStore'
import AssetStore from './stores/AssetsStore'



const {width,height} = Dimensions.get('window')
const assets = new AssetStore()
const settings = new SetStore()

export default class AppContainer extends Component {
  constructor(props) {
    super(props)
    YellowBox.ignoreWarnings(["Setting a timer","Warning: isMounted","Warning: Can't call","unknown"])
    this.state = {
      stores:{
        assets: assets,
        settings: settings,
        // auth: auth
      }
    }
  }
    

  render() {
    return (
      <Root>
        <NavigationContainer 
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef)
        }}
        screenProps={{
          stores: this.state.stores,
          theme: getTheme(platform),
          width: width,
          height:height,
        }}
        />
      </Root>
    )
  }
}
